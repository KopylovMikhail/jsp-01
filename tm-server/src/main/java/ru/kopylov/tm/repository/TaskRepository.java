package ru.kopylov.tm.repository;

import lombok.NoArgsConstructor;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.api.repository.ITaskRepository;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.Task;

import javax.persistence.EntityManager;
import java.util.List;

@Component
@NoArgsConstructor
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final EntityManager manager) {
        super(manager);
    }

    @Override
    public void remove(@NotNull final String id) {
        manager.remove(manager.find(Task.class, id));
    }

    @NotNull
    @Override
    public Task findOne(@NotNull final String id) {
        return manager.find(Task.class, id);
    }

    @NotNull
    public List<Task> findAll() {
        return manager.createQuery("select t from Task t", Task.class)
                .setHint(QueryHints.HINT_CACHEABLE, "true")
                .getResultList();
    }

    @NotNull
    public List<Task> findAllByUserId(@NotNull final String userId) {
        return manager.createQuery("select t from Task t where t.user.id = :userId", Task.class)
                .setHint(QueryHints.HINT_CACHEABLE, "true")
                .setParameter("userId", userId).getResultList();
    }

    @NotNull
    public List<Task> findAllByUserIdOrderByDateStart(@NotNull final String userId) {
        return manager
                .createQuery("select t from Task t where t.user.id = :userId order by t.dateStart asc", Task.class)
                .setHint(QueryHints.HINT_CACHEABLE, "true")
                .setParameter("userId", userId).getResultList();
    }

    @NotNull
    public List<Task> findAllByUserIdOrderByDateFinish(@NotNull final String userId) {
        return manager
                .createQuery("select t from Task t where t.user.id = :userId order by t.dateFinish asc", Task.class)
                .setHint(QueryHints.HINT_CACHEABLE, "true")
                .setParameter("userId", userId).getResultList();
    }

    @NotNull
    public List<Task> findAllByUserIdOrderByState(@NotNull final String userId) {
        return manager
                .createQuery("select t from Task t where t.user.id = :userId order by t.state asc", Task.class)
                .setHint(QueryHints.HINT_CACHEABLE, "true")
                .setParameter("userId", userId).getResultList();
    }

    public void removeAllByUserId(@NotNull final String userId) {
        @NotNull final List<Task> tasks = findAllByUserId(userId);
        for (@NotNull final Task task : tasks) {
            manager.remove(task);
        }
    }

    @NotNull
    public List<Task> findByContent(@NotNull final String content) {
        return manager.createQuery("select t from Task t where t.name like concat('%', :content, '%') " +
                "or t.description like concat('%', :content, '%')", Task.class)
                .setHint(QueryHints.HINT_CACHEABLE, "true")
                .setParameter("content", content).getResultList();
    }

    @NotNull
    public List<Task> findAllByProjectId(@NotNull final String projectId) {
        return manager.createQuery("select t from Task t where t.project.id = :projectId", Task.class)
                .setHint(QueryHints.HINT_CACHEABLE, "true")
                .setParameter("projectId", projectId).getResultList();
    }

    public void setProjectId(
            @NotNull final String projectId,
            @NotNull final String taskId
    ) {
        @NotNull final Task task = manager.find(Task.class, taskId);
        @NotNull final Project project = manager.find(Project.class, projectId);
        task.setProject(project);
        manager.merge(task);
    }

}
