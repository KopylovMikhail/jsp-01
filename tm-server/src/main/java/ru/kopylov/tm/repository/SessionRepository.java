package ru.kopylov.tm.repository;

import lombok.NoArgsConstructor;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.api.repository.ISessionRepository;
import ru.kopylov.tm.entity.Session;

import javax.persistence.EntityManager;
import java.util.List;

@Component
@NoArgsConstructor
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager manager) {
        super(manager);
    }

    @Override
    public void remove(@NotNull final String id) {
        manager.remove(manager.find(Session.class, id));
    }

    @NotNull
    @Override
    public Session findOne(@NotNull final String id) {
        return manager.find(Session.class, id);
    }

    public void removeByUserId(@NotNull final String userId) {
        @NotNull final List<Session> sessions = manager
                .createQuery("select s from Session s where s.user.id = :userId", Session.class)
                .setHint(QueryHints.HINT_CACHEABLE, "true")
                .setParameter("userId", userId).getResultList();
        for (@NotNull final Session session : sessions) {
            manager.remove(session);
        }
    }

}
