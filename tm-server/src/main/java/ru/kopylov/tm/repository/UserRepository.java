package ru.kopylov.tm.repository;

import lombok.NoArgsConstructor;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.api.repository.IUserRepository;
import ru.kopylov.tm.entity.User;

import javax.persistence.EntityManager;
import java.util.List;

@Component
@NoArgsConstructor
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final EntityManager manager) {
        super(manager);
    }

    @Override
    public void remove(@NotNull final String id) {
        manager.remove(manager.find(User.class, id));
    }

    @NotNull
    @Override
    public User findOne(@NotNull final String id) {
        return manager.find(User.class, id);
    }

    @NotNull
    public User findOne(@NotNull final String login, @NotNull final String password) {
        return manager.createQuery("select u from User u where u.login = :login and u.password = :password", User.class)
                .setHint(QueryHints.HINT_CACHEABLE, "true")
                .setParameter("login", login).setParameter("password", password).getSingleResult();
    }

    @NotNull
    public List<User> findAll() {
        return manager.createQuery("select u from User u", User.class)
                .setHint(QueryHints.HINT_CACHEABLE, "true")
                .getResultList();
    }

}
