package ru.kopylov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.api.endpoint.IDataEndpoint;
import ru.kopylov.tm.api.service.IDataService;
import ru.kopylov.tm.api.service.IUserService;
import ru.kopylov.tm.entity.Session;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.enumerated.TypeRole;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Getter
@Component
@NoArgsConstructor
@WebService(endpointInterface = "ru.kopylov.tm.api.endpoint.IDataEndpoint")
public final class DataEndpoint extends AbstractEndpoint implements IDataEndpoint {

    @NotNull
    @Autowired
    private IDataService dataService;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    private final String url = this.getClass().getSimpleName() + "?wsdl";

    @WebMethod
    public void saveDataBin(
            @WebParam(name = "token") @Nullable final String token
    ) throws Exception {
        @Nullable final Session session = sessionService.decryptToken(token);
        validate(session);
        dataService.saveDataBin();
    }

    @WebMethod
    public void loadDataBin(
            @WebParam(name = "token") @Nullable final String token
    ) throws Exception {
        @Nullable final Session session = sessionService.decryptToken(token);
        validate(session);
        dataService.loadDataBin();
    }

    @WebMethod
    public void saveDataJson(
            @WebParam(name = "token") @Nullable final String token
    ) throws Exception {
        @Nullable final Session session = sessionService.decryptToken(token);
        validate(session);
        dataService.saveDataJson();
    }

    @WebMethod
    public void loadDataJson(
            @WebParam(name = "token") @Nullable final String token
    ) throws Exception {
        @Nullable final Session session = sessionService.decryptToken(token);
        validate(session);
        dataService.loadDataJson();
    }

    @WebMethod
    public void saveDataJsonJaxb(
            @WebParam(name = "token") @Nullable final String token
    ) throws Exception {
        @Nullable final Session session = sessionService.decryptToken(token);
        validate(session);
        dataService.saveDataJsonJaxb();
    }

    @WebMethod
    public void loadDataJsonJaxb(
            @WebParam(name = "token") @Nullable final String token
    ) throws Exception {
        @Nullable final Session session = sessionService.decryptToken(token);
        validate(session);
        dataService.loadDataJsonJaxb();
    }

    @WebMethod
    public void saveDataXml(
            @WebParam(name = "token") @Nullable final String token
    ) throws Exception {
        @Nullable final Session session = sessionService.decryptToken(token);
        validate(session);
        dataService.saveDataXml();
    }

    @WebMethod
    public void loadDataXml(
            @WebParam(name = "token") @Nullable final String token
    ) throws Exception {
        @Nullable final Session session = sessionService.decryptToken(token);
        validate(session);
        dataService.loadDataXml();
    }

    @WebMethod
    public void saveDataXmlJaxb(
            @WebParam(name = "token") @Nullable final String token
    ) throws Exception {
        @Nullable final Session session = sessionService.decryptToken(token);
        validate(session);
        dataService.saveDataXmlJaxb();
    }

    @WebMethod
    public void loadDataXmlJaxb(
            @WebParam(name = "token") @Nullable final String token
    ) throws Exception {
        @Nullable final Session session = sessionService.decryptToken(token);
        validate(session);
        dataService.loadDataXmlJaxb();
    }

    private void validate(@Nullable Session session) throws Exception {
        sessionService.validate(session);
        @Nullable final String userId = session.getUser().getId();
        @Nullable User user = userService.findOne(userId);
        if (user == null || user.getRole() != TypeRole.ADMIN) throw new Exception("User does not have enough rights.");
    }

}
