package ru.kopylov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.api.endpoint.IProjectEndpoint;
import ru.kopylov.tm.api.service.IProjectService;
import ru.kopylov.tm.dto.ProjectDto;
import ru.kopylov.tm.dto.TaskDto;
import ru.kopylov.tm.entity.Session;
import ru.kopylov.tm.enumerated.State;
import ru.kopylov.tm.util.EntityToDtoUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Getter
@Component
@NoArgsConstructor
@WebService(endpointInterface = "ru.kopylov.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    private final String url = this.getClass().getSimpleName() + "?wsdl";

    @WebMethod
    public void clearProject(
            @WebParam(name = "token") @Nullable final String token
    ) throws Exception {
        @Nullable final Session session = sessionService.decryptToken(token);
        sessionService.validate(session);
        projectService.removeAll(session.getUser().getId());
    }

    @WebMethod
    public boolean createProject(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "projectName") @Nullable final String projectName
    ) throws Exception {
        @Nullable final Session session = sessionService.decryptToken(token);
        sessionService.validate(session);
        return projectService.persist(session.getUser().getId(), projectName);
    }

    @NotNull
    @WebMethod
    public List<ProjectDto> findProjectContent(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "findWord") @Nullable final String findWord
    ) throws Exception {
        @Nullable final Session session = sessionService.decryptToken(token);
        sessionService.validate(session);
        return EntityToDtoUtil.getProjectList(projectService.findByContent(findWord));
    }

    @Nullable
    @WebMethod
    public List<ProjectDto> getProjectList(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "typeSort") @Nullable final String typeSort
    ) throws Exception {
        @Nullable final Session session = sessionService.decryptToken(token);
        sessionService.validate(session);
        return EntityToDtoUtil.getProjectList(projectService.findAll(session.getUser().getId(), typeSort));
    }

    @WebMethod
    public boolean removeProject(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "projectNumber") @NotNull final Integer projectNumber
    ) throws Exception {
        @Nullable final Session session = sessionService.decryptToken(token);
        sessionService.validate(session);
        return projectService.remove(session.getUser().getId(), projectNumber);
    }

    @WebMethod
    public boolean setProjectTask(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "projectNumber") @NotNull final Integer projectNumber,
            @WebParam(name = "taskNumber") @NotNull final Integer taskNumber
    ) throws Exception {
        @Nullable final Session session = sessionService.decryptToken(token);
        sessionService.validate(session);
        return projectService.setTask(session.getUser().getId(), projectNumber, taskNumber);
    }

    @NotNull
    @WebMethod
    public List<TaskDto> getProjectTaskList(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "projectNumber") @NotNull final Integer projectNumber
    ) throws Exception {
        @Nullable final Session session = sessionService.decryptToken(token);
        sessionService.validate(session);
        return EntityToDtoUtil.getTaskList(projectService.getTaskList(session.getUser().getId(), projectNumber));
    }

    @WebMethod
    public boolean updateProject(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "projectNumber") @NotNull final Integer projectNumber,
            @WebParam(name = "projectName") @Nullable final String projectName,
            @WebParam(name = "projectDescription") @Nullable final String projectDescription,
            @WebParam(name = "projectDateStart") @Nullable final String projectDateStart,
            @WebParam(name = "projectDateFinish") @Nullable final String projectDateFinish,
            @WebParam(name = "stateNumber") @Nullable final Integer stateNumber
    ) throws Exception {
        @Nullable final Session session = sessionService.decryptToken(token);
        sessionService.validate(session);
        return projectService.merge(
                session.getUser().getId(),
                projectNumber,
                projectName,
                projectDescription,
                projectDateStart,
                projectDateFinish,
                stateNumber
        );
    }

    @NotNull
    @WebMethod
    public State[] getStateList() {
        return State.values();
    }

}
