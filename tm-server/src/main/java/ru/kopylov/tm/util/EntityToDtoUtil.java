package ru.kopylov.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.dto.ProjectDto;
import ru.kopylov.tm.dto.SessionDto;
import ru.kopylov.tm.dto.TaskDto;
import ru.kopylov.tm.dto.UserDto;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.Session;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.entity.User;

import java.util.ArrayList;
import java.util.List;

public final class EntityToDtoUtil {

    @NotNull
    public static UserDto getUser(User user) {
        @NotNull final UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setLogin(user.getLogin());
        userDto.setPassword(user.getPassword());
        userDto.setRole(user.getRole());
        return userDto;
    }

    @NotNull
    public static SessionDto getSession(Session session) {
        @NotNull final SessionDto sessionDto = new SessionDto();
        sessionDto.setId(session.getId());
        sessionDto.setTimestamp(session.getTimestamp());
        sessionDto.setUserId(session.getUser().getId());
        sessionDto.setSignature(session.getSignature());
        return sessionDto;
    }

    @NotNull
    public static ProjectDto getProject(Project project) {
        @NotNull final ProjectDto projectDto = new ProjectDto();
        projectDto.setId(project.getId());
        projectDto.setName(project.getName());
        projectDto.setDescription(project.getDescription());
        projectDto.setDateStart(project.getDateStart());
        projectDto.setDateFinish(project.getDateFinish());
        projectDto.setUserId(project.getUser().getId());
        projectDto.setState(project.getState());
        return projectDto;
    }

    @NotNull
    public static TaskDto getTask(Task task) {
        @NotNull final TaskDto taskDto = new TaskDto();
        taskDto.setId(task.getId());
        taskDto.setName(task.getName());
        taskDto.setDescription(task.getDescription());
        taskDto.setDateStart(task.getDateStart());
        taskDto.setDateFinish(task.getDateFinish());
        if (task.getProject() != null && task.getProject().getId() != null)  taskDto.setProjectId(task.getProject().getId());
        taskDto.setUserId(task.getUser().getId());
        taskDto.setState(task.getState());
        return taskDto;
    }

    @NotNull
    public static List<UserDto> getUserList(List<User> users) {
        @NotNull final List<UserDto> usersDto = new ArrayList<>();
        for (@NotNull final User user : users) {
            usersDto.add(getUser(user));
        }
        return usersDto;
    }

    @NotNull
    public static List<ProjectDto> getProjectList(List<Project> projects) {
        @NotNull final List<ProjectDto> projectsDto = new ArrayList<>();
        for (@NotNull final Project project : projects) {
            projectsDto.add(getProject(project));
        }
        return projectsDto;
    }

    @NotNull
    public static List<TaskDto> getTaskList(List<Task> tasks) {
        @NotNull final List<TaskDto> tasksDto = new ArrayList<>();
        for (@NotNull final Task task : tasks) {
            tasksDto.add(getTask(task));
        }
        return tasksDto;
    }

}
