package ru.kopylov.tm.util;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.Session;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.service.PropertyService;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.Map;

public final class HibernateUtil {

    private static final PropertyService propertyService = new PropertyService();

    public static EntityManagerFactory factory() {
//        final Map<String, Object> settings = new HashMap<>();
//        settings.put(Environment.DRIVER, propertyService.getDbDriver());
//        settings.put(Environment.URL, propertyService.getDbHost());
//        settings.put(Environment.USER, propertyService.getDbLogin());
//        settings.put(Environment.PASS, propertyService.getDbPassword());
//        settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5InnoDBDialect");
//        settings.put(Environment.HBM2DDL_AUTO, "update");
//        settings.put(Environment.SHOW_SQL, "true");
//
//        final StandardServiceRegistryBuilder registryBuilder
//                = new StandardServiceRegistryBuilder();
//        registryBuilder.applySettings(settings);
//        final StandardServiceRegistry registry = registryBuilder.build();
//        final MetadataSources sources = new MetadataSources(registry);
//        sources.addAnnotatedClass(Task.class);
//        sources.addAnnotatedClass(Project.class);
//        sources.addAnnotatedClass(User.class);
//        sources.addAnnotatedClass(Session.class);
//        final Metadata metadata = sources.getMetadataBuilder().build();
//        return metadata.getSessionFactoryBuilder().build();
        return Persistence.createEntityManagerFactory("ENTERPRISE");
    }

}
