package ru.kopylov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NoArgsConstructor;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.api.service.*;
import ru.kopylov.tm.constant.DataPath;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.RootEntity;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.entity.User;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.util.List;

@Component
@NoArgsConstructor
public class DataService implements IDataService {

    @Autowired
    private IUserService userService;

    @Autowired
    private IProjectService projectService;

    @Autowired
    private ITaskService taskService;

    public void saveDataBin() throws Exception {
        @NotNull final List<User> users = userService.findAll();
        @NotNull final List<Project> projects = projectService.findAll();
        @NotNull final List<Task> tasks = taskService.findAll();
        @NotNull final File file = new File(DataPath.PATH_BIN);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(users);
        objectOutputStream.writeObject(projects);
        objectOutputStream.writeObject(tasks);
        objectOutputStream.close();
        fileOutputStream.close();
    }

    public void loadDataBin() throws Exception {
        @NotNull final File file = new File(DataPath.PATH_BIN);
        @NotNull final FileInputStream fileInputStream = new FileInputStream(file);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        loadUsers(objectInputStream.readObject());
        loadProjects(objectInputStream.readObject());
        loadTasks(objectInputStream.readObject());
        objectInputStream.close();
        fileInputStream.close();
    }

    public void saveDataJson() throws Exception {
        @NotNull final List<User> users = userService.findAll();
        @NotNull final List<Project> projects = projectService.findAll();
        @NotNull final List<Task> tasks = taskService.findAll();
        @NotNull final RootEntity root = new RootEntity();
        root.setUsers(users);
        root.setProjects(projects);
        root.setTasks(tasks);
        @NotNull final File file = new File(DataPath.PATH_FASTERXML_JSON);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(file, root);
    }

    public void loadDataJson() throws Exception {
        @NotNull final File file = new File(DataPath.PATH_FASTERXML_JSON);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final RootEntity root = objectMapper.readValue(file, RootEntity.class);
        for (@NotNull final User user : root.getUsers()) {
            userService.merge(user);
        }
        for (@NotNull final Project project : root.getProjects()) {
            projectService.merge(project);
        }
        for (@NotNull final Task task : root.getTasks()) {
            taskService.merge(task);
        }
    }

    public void saveDataJsonJaxb() throws Exception {
        @NotNull final List<User> users = userService.findAll();
        @NotNull final List<Project> projects = projectService.findAll();
        @NotNull final List<Task> tasks = taskService.findAll();
        @NotNull final RootEntity root = new RootEntity();
        root.setUsers(users);
        root.setProjects(projects);
        root.setTasks(tasks);
        @NotNull final File file = new File(DataPath.PATH_JAXB_JSON);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(RootEntity.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(root, file);
    }

    public void loadDataJsonJaxb() throws Exception {
        @NotNull final File file = new File(DataPath.PATH_JAXB_JSON);
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(RootEntity.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        unmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final RootEntity root = (RootEntity) unmarshaller.unmarshal(file);
        for (@NotNull final User user : root.getUsers()) {
            userService.merge(user);
        }
        for (@NotNull final Project project : root.getProjects()) {
            projectService.merge(project);
        }
        for (@NotNull final Task task : root.getTasks()) {
            taskService.merge(task);
        }
    }

    public void saveDataXml() throws Exception {
        @NotNull final List<User> users = userService.findAll();
        @NotNull final List<Project> projects = projectService.findAll();
        @NotNull final List<Task> tasks = taskService.findAll();
        @NotNull final RootEntity root = new RootEntity();
        root.setUsers(users);
        root.setProjects(projects);
        root.setTasks(tasks);
        @NotNull final File file = new File(DataPath.PATH_FASTERXML_XML);
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(file, root);
    }

    public void loadDataXml() throws Exception {
        @NotNull final File file = new File(DataPath.PATH_FASTERXML_XML);
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final RootEntity root = xmlMapper.readValue(file, RootEntity.class);
        for (@NotNull final User user : root.getUsers()) {
            userService.merge(user);
        }
        for (@NotNull final Project project : root.getProjects()) {
            projectService.merge(project);
        }
        for (@NotNull final Task task : root.getTasks()) {
            taskService.merge(task);
        }
    }

    public void saveDataXmlJaxb() throws Exception {
        @NotNull final List<User> users = userService.findAll();
        @NotNull final List<Project> projects = projectService.findAll();
        @NotNull final List<Task> tasks = taskService.findAll();
        @NotNull final RootEntity root = new RootEntity();
        root.setUsers(users);
        root.setProjects(projects);
        root.setTasks(tasks);
        @NotNull final File file = new File(DataPath.PATH_JAXB_XML);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(RootEntity.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(root, file);
    }

    public void loadDataXmlJaxb() throws Exception {
        @NotNull final File file = new File(DataPath.PATH_JAXB_XML);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(RootEntity.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @NotNull final RootEntity root = (RootEntity) unmarshaller.unmarshal(file);
        for (@NotNull final User user : root.getUsers()) {
            userService.merge(user);
        }
        for (@NotNull final Project project : root.getProjects()) {
            projectService.merge(project);
        }
        for (@NotNull final Task task : root.getTasks()) {
            taskService.merge(task);
        }
    }

    private void loadUsers(@Nullable final Object object) throws Exception {
        if (!(object instanceof List<?>)) return;
        if (((List) object).isEmpty()) return;
        if (!(((List) object).get(0) instanceof User)) return; //если первый объект листа не принадлежит типу User, то выход из метода
        @NotNull final List<User> users = (List<User>) object;
        for (@NotNull final User user : users) {
            userService.merge(user);
        }
    }

    private void loadProjects(@Nullable final Object object) throws Exception {
        if (!(object instanceof List<?>)) return;
        if (((List) object).isEmpty()) return;
        if (!(((List) object).get(0) instanceof Project)) return; //если первый объект листа не принадлежит типу Project, то выход из метода
        @NotNull final List<Project> projects = (List<Project>) object;
        for (@NotNull final Project project : projects) {
            projectService.merge(project);
        }
    }

    private void loadTasks(@Nullable final Object object) throws Exception {
        if (!(object instanceof List<?>)) return;
        if (((List) object).isEmpty()) return;
        if (!(((List) object).get(0) instanceof Task)) return; //если первый объект листа не принадлежит типу Task, то выход из метода
        @NotNull final List<Task> tasks = (List<Task>) object;
        for (@NotNull final Task task : tasks) {
            taskService.merge(task);
        }
    }

}
