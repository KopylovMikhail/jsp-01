package ru.kopylov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.kopylov.tm.api.repository.IUserRepository;
import ru.kopylov.tm.api.service.IUserService;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.enumerated.TypeRole;
import ru.kopylov.tm.util.HashUtil;

import java.util.List;

@Component
@Transactional
@NoArgsConstructor
public class UserService extends AbstractService implements IUserService {

    @NotNull
    @Autowired
    private IUserRepository userRepository;

    public boolean merge(@Nullable final User user) {
        if (user == null) return false;
        if (user.getId() == null || user.getId().isEmpty()) return false;
        if (user.getLogin() == null || user.getLogin().isEmpty()) return false;
        if (user.getPassword() == null || user.getPassword().isEmpty()) return false;
        userRepository.merge(user);
        return true;
    }

    public boolean merge(
            @NotNull final String userId,
            @NotNull final String login,
            @NotNull final String password
    ) {
        @NotNull final User user = findOne(userId);
        @NotNull final String hashPassword = HashUtil.hash(password);
        user.setLogin(login);
        user.setPassword(hashPassword);
        return merge(user);
    }

    public boolean persist(@Nullable final User user) {
        if (user == null) return false;
        userRepository.persist(user);
        return true;
    }

    public boolean persist(@NotNull final String login, @NotNull final String password) {
        if (password.isEmpty() || login.isEmpty()) return false;
        @NotNull final String hashPassword = HashUtil.hash(password);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPassword(hashPassword);
        user.setRole(TypeRole.USER);
        userRepository.persist(user);
        return true;
    }

    @Nullable
    public User findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return userRepository.findOne(id);
    }

    @Nullable
    public User findOne(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        @NotNull final String hashPassword = HashUtil.hash(password);
        return userRepository.findOne(login, hashPassword);
    }

    @NotNull
    public List<User> findAll() {
        return userRepository.findAll();
    }

    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        userRepository.remove(id);
    }

}
