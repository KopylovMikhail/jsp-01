package ru.kopylov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.kopylov.tm.api.repository.ITaskRepository;
import ru.kopylov.tm.api.service.ITaskService;
import ru.kopylov.tm.api.service.IUserService;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.enumerated.State;
import ru.kopylov.tm.enumerated.TypeSort;
import ru.kopylov.tm.util.DateUtil;

import java.util.Collections;
import java.util.Date;
import java.util.List;

@Component
@Transactional
@NoArgsConstructor
public class TaskService extends AbstractService implements ITaskService {

    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @Autowired
    private IUserService userService;

    public boolean persist(@Nullable final Task task) {
        if (task == null ) return false;
        taskRepository.persist(task);
        return true;
    }

    public boolean persist(@Nullable final String currentUserId, @Nullable final String taskName) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        @NotNull final Task task = new Task();
        task.setName(taskName);
        @Nullable final User user = userService.findOne(currentUserId);
        task.setUser(user);
        task.setDateStart(new Date());
        task.setDateFinish(new Date());
        return persist(task);
    }

    @Nullable
    public Task findOne(@Nullable final String taskId) {
        if (taskId == null || taskId.isEmpty()) return null;
        return taskRepository.findOne(taskId);
    }

    @NotNull
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @NotNull
    public List<Task> findAll(@Nullable final String currentUserId) {
        if (currentUserId == null || currentUserId.isEmpty()) return Collections.emptyList();
        return taskRepository.findAllByUserId(currentUserId);
    }

    @NotNull
    public List<Task> findAllByProjectId(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }

    @Nullable
    public List<Task> findAll(@Nullable final String currentUserId, @Nullable final String typeSort) {
        if (currentUserId == null || currentUserId.isEmpty()) return null;
        if (TypeSort.CREATE_DATE.getDisplayName().equals(typeSort) || typeSort == null || typeSort.isEmpty())
            return taskRepository.findAllByUserId(currentUserId);
        if (TypeSort.START_DATE.getDisplayName().equals(typeSort)) {
            return taskRepository.findAllByUserIdOrderByDateStart(currentUserId);
        }
        if (TypeSort.FINISH_DATE.getDisplayName().equals(typeSort)) {
            return taskRepository.findAllByUserIdOrderByDateFinish(currentUserId);
        }
        if (TypeSort.STATE.getDisplayName().equals(typeSort)) {
            return taskRepository.findAllByUserIdOrderByState(currentUserId);
        }
        return null;
    }

    public boolean merge(@Nullable final Task task) {
        if (task == null ) return false;
        if (task.getId() == null || task.getId().isEmpty()) return false;
        taskRepository.merge(task);
        return true;
    }

    public boolean merge(
            @Nullable final String currentUserId,
            @NotNull final Integer taskNumber,
            @Nullable final String taskName,
            @Nullable final String taskDescription,
            @Nullable final String taskDateStart,
            @Nullable final String taskDateFinish,
            @Nullable final Integer stateNumber
    ) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        @NotNull final List<Task> taskList = taskRepository.findAllByUserId(currentUserId);
        if (taskNumber < 1 || taskNumber > taskList.size()) return false;
        @NotNull final Task task = taskList.get(taskNumber - 1);
        if (taskName != null && !taskName.isEmpty()) task.setName(taskName);
        if (taskDescription != null && !taskDescription.isEmpty())
            task.setDescription(taskDescription);
        if (taskDateStart != null && !taskDateStart.isEmpty()) {
            @NotNull final Date dateStart = DateUtil.stringToDate(taskDateStart);
            task.setDateStart(dateStart);
        }
        if (taskDateFinish != null && !taskDateFinish.isEmpty()) {
            @NotNull final Date dateFinish = DateUtil.stringToDate(taskDateFinish);
            task.setDateFinish(dateFinish);
        }
        if (stateNumber != null) {
            if (stateNumber < 1 || stateNumber > State.values().length) return false;
            task.setState(State.values()[stateNumber-1]);
        }
        return merge(task);
    }

    public boolean remove(@Nullable final String taskId) {
        if (taskId == null || taskId.isEmpty()) return false;
        taskRepository.remove(taskId);
        return true;
    }

    public boolean remove(@Nullable final String currentUserId, @NotNull final Integer taskNumber) {
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        @NotNull final List<Task> taskList = findAll(currentUserId);
        if (taskNumber < 1 || taskNumber > taskList.size()) return false;
        @NotNull final Task task = taskList.get(taskNumber - 1);
        return remove(task.getId());
    }

    public void removeAll(@Nullable final String currentUserId) {
        if (currentUserId == null || currentUserId.isEmpty()) return;
        taskRepository.removeAllByUserId(currentUserId);
    }

    @NotNull
    public List<Task> findByContent(@Nullable String content) {
        if (content == null || content.isEmpty()) return Collections.emptyList();
        return taskRepository.findByContent(content);
    }

}
