package ru.kopylov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.api.service.IPropertyService;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Component
@NoArgsConstructor
public final class PropertyService implements IPropertyService {

    @NotNull
    private static final Properties properties = new Properties();

    @PostConstruct
    public void init() throws IOException {
        @Nullable final InputStream inputStream = PropertyService.class.getClassLoader()
                .getResourceAsStream("application.properties");
        properties.load(inputStream);
    }

    @NotNull
    public final String getPort() {
        return properties.getProperty("port");
    }

    @NotNull
    public final String getHost() {
        return properties.getProperty("host");
    }

    @NotNull
    public final String getSalt() {
        return properties.getProperty("salt");
    }

    public final int getCycle() {
        return Integer.parseInt(properties.getProperty("cycle"));
    }

    public final long getLifeTime() {
        return Long.parseLong(properties.getProperty("sessionLifeTime"));
    }

    @NotNull
    public final String getDbDriver() {
        return properties.getProperty("db.driver");
    }

    @NotNull
    public final String getDbHost() {
        return properties.getProperty("db.host");
    }

    @NotNull
    public final String getDbLogin() {
        return properties.getProperty("db.login");
    }

    @NotNull
    public final String getDbPassword() {
        return properties.getProperty("db.password");
    }

    @NotNull
    public final String getSecretKey() {
        return properties.getProperty("secretKey");
    }

}
