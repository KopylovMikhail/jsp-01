package ru.kopylov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.enumerated.TypeRole;

@Getter
@Setter
@NoArgsConstructor
public final class UserDto extends AbstractDto {

    @Nullable
    private String login;

    @Nullable
    private String password;

    @Nullable
    private TypeRole role;

}
