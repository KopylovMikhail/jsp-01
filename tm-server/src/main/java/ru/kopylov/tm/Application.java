package ru.kopylov.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.kopylov.tm.config.ApplicationConfig;
import ru.kopylov.tm.context.Bootstrap;

/**
 * @author Mikhail Kopylov
 * task/project manager
 */

public class Application {

    public static void main(String[] args) throws Exception {
        @NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.init();
    }

}
