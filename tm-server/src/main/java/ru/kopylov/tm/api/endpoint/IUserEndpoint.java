package ru.kopylov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.dto.UserDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IUserEndpoint {

    @NotNull
    String getUrl();

    @WebMethod
    boolean persistUser(
            @WebParam(name = "login") @NotNull String login,
            @WebParam(name = "password") @NotNull String password
    ) throws Exception;

    @Nullable
    @WebMethod
    String loginUser(
            @WebParam(name = "login") @NotNull String login,
            @WebParam(name = "password") @NotNull String password
    ) throws Exception;

    @WebMethod
    void logoutUser(
            @WebParam(name = "token") @Nullable String token
    ) throws Exception;

    @Nullable
    @WebMethod
    public UserDto getUserProfile(
            @WebParam(name = "token") @Nullable String token
    ) throws Exception;

    @WebMethod
    void updateUser(
            @WebParam(name = "token") @Nullable String token,
            @WebParam(name = "login") @NotNull String login,
            @WebParam(name = "password") @NotNull String password
    ) throws Exception;

    @WebMethod
    void removeUser(
            @WebParam(name = "token") @Nullable String token,
            @WebParam(name = "id") @NotNull String id
    ) throws Exception;

    @NotNull
    @WebMethod
    List<UserDto> getUserList(
            @WebParam(name = "token") @Nullable String token
    ) throws Exception;

    @WebMethod
    boolean removeSession(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "userId") @NotNull final String userId
    ) throws Exception;

}
