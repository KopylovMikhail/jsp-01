package ru.kopylov.tm.api.service;

import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;

public interface ServiceLocator {

    @NotNull
    IProjectService getProjectService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IDataService getDataService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    ITerminalService getTerminalService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    Connection getConnection() throws Exception;

    @NotNull
    SqlSessionFactory getSqlSessionFactory();

}
