package ru.kopylov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.entity.User;

import java.util.List;

public interface IUserService {

    boolean merge(@Nullable User user) throws Exception;

    boolean persist(@Nullable User user) throws Exception;

    boolean merge(
            @NotNull String currentUserId,
            @NotNull String login,
            @NotNull String password
    ) throws Exception;

    boolean persist(@NotNull String login, @NotNull String password) throws Exception;

    @Nullable
    User findOne(@Nullable String id) throws Exception;

    @Nullable
    User findOne(@Nullable String login, @Nullable String password);

    @NotNull
    List<User> findAll() throws Exception;

    void remove(@Nullable String id);

}
