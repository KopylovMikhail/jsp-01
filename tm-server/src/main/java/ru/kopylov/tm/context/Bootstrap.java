package ru.kopylov.tm.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.api.endpoint.IDataEndpoint;
import ru.kopylov.tm.api.endpoint.IProjectEndpoint;
import ru.kopylov.tm.api.endpoint.ITaskEndpoint;
import ru.kopylov.tm.api.endpoint.IUserEndpoint;
import ru.kopylov.tm.api.service.IPropertyService;
import ru.kopylov.tm.api.service.ITerminalService;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.enumerated.TypeRole;
import ru.kopylov.tm.repository.UserRepository;
import ru.kopylov.tm.util.HashUtil;
import ru.kopylov.tm.util.HibernateUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.xml.ws.Endpoint;

@Getter
@Component
@NoArgsConstructor
public final class Bootstrap {

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private IDataEndpoint dataEndpoint;

    @NotNull
    @Autowired
    private ITerminalService terminalService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    public void init() {
        try {
//            propertyService.init();
//            userInit();
        }catch (Exception e) {
            e.printStackTrace();
        }
        endpointRegistry();
    }

    private void endpointRegistry() {
        @NotNull final String host = propertyService.getHost();
        @NotNull final String port = propertyService.getPort();
        @NotNull final String url = String.format("http://%s:%s/", host, port);
        Endpoint.publish(url + userEndpoint.getUrl(), userEndpoint);
        System.out.println(url + userEndpoint.getUrl());
        Endpoint.publish(url + projectEndpoint.getUrl(), projectEndpoint);
        System.out.println(url + projectEndpoint.getUrl());
        Endpoint.publish(url + taskEndpoint.getUrl(), taskEndpoint);
        System.out.println(url + taskEndpoint.getUrl());
        Endpoint.publish(url + dataEndpoint.getUrl(), dataEndpoint);
        System.out.println(url + dataEndpoint.getUrl());

    }

    private void userInit() throws Exception {
        final EntityManagerFactory factory = HibernateUtil.factory();
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();

        final User test = new User();
        test.setId("7966e8d4-1fd9-4665-aa1a-ae3a5c5f5b31");
        test.setLogin("test");
        test.setRole(TypeRole.ADMIN);
        test.setPassword(HashUtil.hash("test"));
        em.merge(test);

        @NotNull final User user = new User();
        user.setId("7ff3bb1a-1f4c-4468-a1f0-2111f578f8cf");
        user.setLogin("user");
        user.setRole(TypeRole.USER);
        user.setPassword(HashUtil.hash("222222"));
        UserRepository userRepository = new UserRepository(em);
        userRepository.merge(user);
        em.getTransaction().commit();

        em.close();
        factory.close();
    }

}
