package ru.kopylov.tm.util;

import org.jetbrains.annotations.NotNull;

import javax.xml.datatype.XMLGregorianCalendar;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public final class DateUtil {

    private final static String TIMESTAMP_PATTERN = "dd.MM.yyyy";

    private final static DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(TIMESTAMP_PATTERN);

    @NotNull
    public static String xgcToString(XMLGregorianCalendar xgc) {
        ZonedDateTime zdt = xgc.toGregorianCalendar().toZonedDateTime();
        return DATE_TIME_FORMATTER.format(zdt);
    }

}
