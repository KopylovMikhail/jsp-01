package ru.kopylov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.api.endpoint.ProjectDto;
import ru.kopylov.tm.api.endpoint.TaskDto;
import ru.kopylov.tm.util.CommandUtil;

import java.util.List;

@Component
@NoArgsConstructor
public final class ProjectTasksListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-tasks-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Project task list.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT'S TASK LIST]");
        @NotNull final List<ProjectDto> projects = projectEndpoint.getProjectList(bootstrap.getToken(), null);
        CommandUtil.printProjectListWithParam(projects);
        System.out.println("ENTER EXISTING PROJECT NUMBER:");
        @Nullable final String terminalCommand = bootstrap.getTerminalService().getReadLine();
        if (terminalCommand == null  || terminalCommand.isEmpty()) {
            System.out.println("Number is empty.");
            return;
        }
        @NotNull final Integer projectNumber = Integer.parseInt(terminalCommand);
        @NotNull final List<TaskDto> tasks = projectEndpoint.getProjectTaskList(bootstrap.getToken(), projectNumber);
        CommandUtil.printTaskListWithParam(tasks);
    }

}
