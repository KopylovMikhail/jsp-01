package ru.kopylov.tm.command.data.json;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.command.AbstractCommand;

@Component
@NoArgsConstructor
public final class LoadFasterxmlJsonCommand extends AbstractCommand {

    @Override
    public @NotNull String getName() {
        return "data-json-load";
    }

    @Override
    public @NotNull String getDescription() {
        return "Load a subject area using FASTERXML from json-format.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON by FASTERXML LOAD]");
        dataEndpoint.loadDataJson(bootstrap.getToken());
        System.out.println("[OK]");
    }

}
