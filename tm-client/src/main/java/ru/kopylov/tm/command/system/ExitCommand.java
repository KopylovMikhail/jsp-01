package ru.kopylov.tm.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.command.AbstractCommand;

@Component
@NoArgsConstructor
public final class ExitCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "exit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Exit from application.";
    }

    @Override
    public void execute() throws Exception {
        System.exit(0);
    }

}
