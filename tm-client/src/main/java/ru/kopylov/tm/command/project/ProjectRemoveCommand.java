package ru.kopylov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.api.endpoint.ProjectDto;
import ru.kopylov.tm.util.CommandUtil;

import java.util.List;

@Component
@NoArgsConstructor
public final class ProjectRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT REMOVE]");
        @NotNull final List<ProjectDto> projects = projectEndpoint.getProjectList(bootstrap.getToken(), null);
        CommandUtil.printProjectListWithParam(projects);
        System.out.println("ENTER EXISTING PROJECT NUMBER:");
        @Nullable final String terminalCommand = bootstrap.getTerminalService().getReadLine();
        if (terminalCommand == null || terminalCommand.isEmpty()) {
            System.out.println("Number is empty.");
            return;
        }
        @NotNull final Integer projectNumber = Integer.parseInt(terminalCommand);
        final boolean removeSuccess = projectEndpoint.removeProject(bootstrap.getToken(), projectNumber);
        if (removeSuccess)
            System.out.println("[PROJECT REMOVED]\n");
        else System.out.println("Such a project does not exist or name is empty.");
    }

}
