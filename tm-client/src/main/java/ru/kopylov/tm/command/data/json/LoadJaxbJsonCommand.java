package ru.kopylov.tm.command.data.json;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.command.AbstractCommand;

@Component
@NoArgsConstructor
public final class LoadJaxbJsonCommand extends AbstractCommand {

    @Override
    public @NotNull String getName() {
        return "data-json-load-jaxb";
    }

    @Override
    public @NotNull String getDescription() {
        return "Load a subject area using JAX-B from json-format.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON by JAX-B LOAD]");
        dataEndpoint.loadDataJsonJaxb(bootstrap.getToken());
        System.out.println("[OK]");
    }

}
