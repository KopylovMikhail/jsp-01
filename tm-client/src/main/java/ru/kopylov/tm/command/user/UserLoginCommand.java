package ru.kopylov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.command.AbstractCommand;

@Component
@NoArgsConstructor
public final class UserLoginCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "login";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "User authorization.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER AUTHORIZATION]\n" +
                "ENTER LOGIN:");
        @Nullable final String login = bootstrap.getTerminalService().getReadLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = bootstrap.getTerminalService().getReadLine();
        if (password == null || password.isEmpty() || login == null || login.isEmpty()) {
            System.out.println("LOGIN OR PASSWORD IS EMPTY.\n");
            return;
        }
        @Nullable final String token = userEndpoint.loginUser(login, password);
        if (token == null || token.isEmpty()) {
            System.out.println("Such user does not exist or login/password is incorrect.");
            return;
        }
        bootstrap.setToken(token);
        System.out.println("[AUTHORIZATION SUCCESS]\n" + "HELLO, " + login + "!");
    }

}
