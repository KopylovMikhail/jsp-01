package ru.kopylov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.api.endpoint.ProjectDto;
import ru.kopylov.tm.endpoint.State;
import ru.kopylov.tm.util.CommandUtil;

import java.util.List;

@Component
@NoArgsConstructor
public final class ProjectUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-update";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update selected project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT UPDATE]");
        @NotNull final List<ProjectDto> projects = projectEndpoint.getProjectList(bootstrap.getToken(), null);
        CommandUtil.printProjectListWithParam(projects);
        System.out.println("ENTER EXISTING PROJECT NUMBER:");
        @Nullable String terminalCommand = bootstrap.getTerminalService().getReadLine();
        if (terminalCommand == null || terminalCommand.isEmpty()) {
            System.out.println("Number is empty.");
            return;
        }
        @NotNull final Integer projectNumber = Integer.parseInt(terminalCommand);
        System.out.println("ENTER NEW PROJECT NAME OR PRESS [ENTER] TO SKIP:");
        @Nullable final String projectName = bootstrap.getTerminalService().getReadLine();
        System.out.println("ENTER PROJECT DESCRIPTION OR PRESS [ENTER] TO SKIP:");
        @Nullable final String projectDescription = bootstrap.getTerminalService().getReadLine();
        System.out.println("ENTER PROJECT DATE START (DD.MM.YYYY) OR PRESS [ENTER] TO SKIP:");
        @Nullable final String projectDateStart = bootstrap.getTerminalService().getReadLine();
        System.out.println("ENTER PROJECT DATE FINISH (DD.MM.YYYY) OR PRESS [ENTER] TO SKIP:");
        @Nullable final String projectDateFinish = bootstrap.getTerminalService().getReadLine();
        @NotNull final State[] states = projectEndpoint.getStateList().toArray(new State[0]);
        int count = 1;
        for (State state : states) {
            System.out.println(count++ + ". " + state.value());
        }
        System.out.println("ENTER PROJECT STATE NUMBER OR PRESS [ENTER] TO SKIP:");
        terminalCommand = bootstrap.getTerminalService().getReadLine();
        @Nullable Integer stateNumber = null;
        if (terminalCommand != null && !terminalCommand.isEmpty()) stateNumber = Integer.parseInt(terminalCommand);
        final boolean updateSuccess = projectEndpoint.updateProject(
                bootstrap.getToken(),
                projectNumber,
                projectName,
                projectDescription,
                projectDateStart,
                projectDateFinish,
                stateNumber
        );
        if (updateSuccess)
            System.out.println("[PROJECT HAS BEEN UPDATED]\n");
        else System.out.println("Such a project does not exist or name is empty.");
    }

}
