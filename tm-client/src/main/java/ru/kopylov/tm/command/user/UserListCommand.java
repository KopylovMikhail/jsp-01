package ru.kopylov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.api.endpoint.UserDto;

import java.util.List;

@Component
@NoArgsConstructor
public class UserListCommand extends AbstractCommand {

    @Override
    public @NotNull String getName() {
        return "user-list";
    }

    @Override
    public @NotNull String getDescription() {
        return "Show all users. Only for administrator.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER LIST]");
        @NotNull final List<UserDto> users = userEndpoint.getUserList(bootstrap.getToken());
        for (int i = 0; i < users.size(); i++) {
            System.out.println(i+1 + ". " + users.get(i).getLogin() + ", " + users.get(i).getRole() + ", " + users.get(i).getId());
        }
    }

}
