package ru.kopylov.tm.command.data.json;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.command.AbstractCommand;

@Component
@NoArgsConstructor
public final class SaveJaxbJsonCommand extends AbstractCommand {

    @Override
    public @NotNull String getName() {
        return "data-json-save-jaxb";
    }

    @Override
    public @NotNull String getDescription() {
        return "Saving a subject area using JAX-B in json-format.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON by JAX-B SAVE]");
        dataEndpoint.saveDataJsonJaxb(bootstrap.getToken());
        System.out.println("[OK]");
    }

}
