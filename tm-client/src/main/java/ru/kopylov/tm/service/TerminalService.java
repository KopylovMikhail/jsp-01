package ru.kopylov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.api.ITerminalService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Component
@NoArgsConstructor
public final class TerminalService implements ITerminalService {

    @NotNull
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    @Nullable
    @Override
    public String getReadLine() throws IOException {
        return reader.readLine();
    }

}
