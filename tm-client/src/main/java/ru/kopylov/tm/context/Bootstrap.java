package ru.kopylov.tm.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.api.ITerminalService;
import ru.kopylov.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Getter
@Setter
@Component
@NoArgsConstructor
public final class Bootstrap {

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Nullable
    private String token = null;

    @NotNull
    @Autowired
    private ITerminalService terminalService;

    public void init(@NotNull Collection<AbstractCommand> commandList) {
        for (@NotNull AbstractCommand command : commandList) {
            registryCommand(command);
        }
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        @Nullable String command = "";
        do {
            try {
                command = terminalService.getReadLine();
                execute(command);
            } catch (Exception e) {
                System.out.println(e.getMessage());
                if (e.getMessage() == null) System.out.println(e.toString());
            }
        } while (!"exit".equals(command));
    }

    private void registryCommand(@NotNull AbstractCommand command) {
            commands.put(command.getName(), command);
    }

    private void execute(@Nullable final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        abstractCommand.execute();
    }

}
