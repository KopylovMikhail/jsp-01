
package ru.kopylov.tm.api.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.kopylov.tm.api.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://endpoint.api.tm.kopylov.ru/", "Exception");
    private final static QName _GetUrl_QNAME = new QName("http://endpoint.api.tm.kopylov.ru/", "getUrl");
    private final static QName _GetUrlResponse_QNAME = new QName("http://endpoint.api.tm.kopylov.ru/", "getUrlResponse");
    private final static QName _LoadDataBin_QNAME = new QName("http://endpoint.api.tm.kopylov.ru/", "loadDataBin");
    private final static QName _LoadDataBinResponse_QNAME = new QName("http://endpoint.api.tm.kopylov.ru/", "loadDataBinResponse");
    private final static QName _LoadDataJson_QNAME = new QName("http://endpoint.api.tm.kopylov.ru/", "loadDataJson");
    private final static QName _LoadDataJsonJaxb_QNAME = new QName("http://endpoint.api.tm.kopylov.ru/", "loadDataJsonJaxb");
    private final static QName _LoadDataJsonJaxbResponse_QNAME = new QName("http://endpoint.api.tm.kopylov.ru/", "loadDataJsonJaxbResponse");
    private final static QName _LoadDataJsonResponse_QNAME = new QName("http://endpoint.api.tm.kopylov.ru/", "loadDataJsonResponse");
    private final static QName _LoadDataXml_QNAME = new QName("http://endpoint.api.tm.kopylov.ru/", "loadDataXml");
    private final static QName _LoadDataXmlJaxb_QNAME = new QName("http://endpoint.api.tm.kopylov.ru/", "loadDataXmlJaxb");
    private final static QName _LoadDataXmlJaxbResponse_QNAME = new QName("http://endpoint.api.tm.kopylov.ru/", "loadDataXmlJaxbResponse");
    private final static QName _LoadDataXmlResponse_QNAME = new QName("http://endpoint.api.tm.kopylov.ru/", "loadDataXmlResponse");
    private final static QName _SaveDataBin_QNAME = new QName("http://endpoint.api.tm.kopylov.ru/", "saveDataBin");
    private final static QName _SaveDataBinResponse_QNAME = new QName("http://endpoint.api.tm.kopylov.ru/", "saveDataBinResponse");
    private final static QName _SaveDataJson_QNAME = new QName("http://endpoint.api.tm.kopylov.ru/", "saveDataJson");
    private final static QName _SaveDataJsonJaxb_QNAME = new QName("http://endpoint.api.tm.kopylov.ru/", "saveDataJsonJaxb");
    private final static QName _SaveDataJsonJaxbResponse_QNAME = new QName("http://endpoint.api.tm.kopylov.ru/", "saveDataJsonJaxbResponse");
    private final static QName _SaveDataJsonResponse_QNAME = new QName("http://endpoint.api.tm.kopylov.ru/", "saveDataJsonResponse");
    private final static QName _SaveDataXml_QNAME = new QName("http://endpoint.api.tm.kopylov.ru/", "saveDataXml");
    private final static QName _SaveDataXmlJaxb_QNAME = new QName("http://endpoint.api.tm.kopylov.ru/", "saveDataXmlJaxb");
    private final static QName _SaveDataXmlJaxbResponse_QNAME = new QName("http://endpoint.api.tm.kopylov.ru/", "saveDataXmlJaxbResponse");
    private final static QName _SaveDataXmlResponse_QNAME = new QName("http://endpoint.api.tm.kopylov.ru/", "saveDataXmlResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.kopylov.tm.api.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link GetUrl }
     * 
     */
    public GetUrl createGetUrl() {
        return new GetUrl();
    }

    /**
     * Create an instance of {@link GetUrlResponse }
     * 
     */
    public GetUrlResponse createGetUrlResponse() {
        return new GetUrlResponse();
    }

    /**
     * Create an instance of {@link LoadDataBin }
     * 
     */
    public LoadDataBin createLoadDataBin() {
        return new LoadDataBin();
    }

    /**
     * Create an instance of {@link LoadDataBinResponse }
     * 
     */
    public LoadDataBinResponse createLoadDataBinResponse() {
        return new LoadDataBinResponse();
    }

    /**
     * Create an instance of {@link LoadDataJson }
     * 
     */
    public LoadDataJson createLoadDataJson() {
        return new LoadDataJson();
    }

    /**
     * Create an instance of {@link LoadDataJsonJaxb }
     * 
     */
    public LoadDataJsonJaxb createLoadDataJsonJaxb() {
        return new LoadDataJsonJaxb();
    }

    /**
     * Create an instance of {@link LoadDataJsonJaxbResponse }
     * 
     */
    public LoadDataJsonJaxbResponse createLoadDataJsonJaxbResponse() {
        return new LoadDataJsonJaxbResponse();
    }

    /**
     * Create an instance of {@link LoadDataJsonResponse }
     * 
     */
    public LoadDataJsonResponse createLoadDataJsonResponse() {
        return new LoadDataJsonResponse();
    }

    /**
     * Create an instance of {@link LoadDataXml }
     * 
     */
    public LoadDataXml createLoadDataXml() {
        return new LoadDataXml();
    }

    /**
     * Create an instance of {@link LoadDataXmlJaxb }
     * 
     */
    public LoadDataXmlJaxb createLoadDataXmlJaxb() {
        return new LoadDataXmlJaxb();
    }

    /**
     * Create an instance of {@link LoadDataXmlJaxbResponse }
     * 
     */
    public LoadDataXmlJaxbResponse createLoadDataXmlJaxbResponse() {
        return new LoadDataXmlJaxbResponse();
    }

    /**
     * Create an instance of {@link LoadDataXmlResponse }
     * 
     */
    public LoadDataXmlResponse createLoadDataXmlResponse() {
        return new LoadDataXmlResponse();
    }

    /**
     * Create an instance of {@link SaveDataBin }
     * 
     */
    public SaveDataBin createSaveDataBin() {
        return new SaveDataBin();
    }

    /**
     * Create an instance of {@link SaveDataBinResponse }
     * 
     */
    public SaveDataBinResponse createSaveDataBinResponse() {
        return new SaveDataBinResponse();
    }

    /**
     * Create an instance of {@link SaveDataJson }
     * 
     */
    public SaveDataJson createSaveDataJson() {
        return new SaveDataJson();
    }

    /**
     * Create an instance of {@link SaveDataJsonJaxb }
     * 
     */
    public SaveDataJsonJaxb createSaveDataJsonJaxb() {
        return new SaveDataJsonJaxb();
    }

    /**
     * Create an instance of {@link SaveDataJsonJaxbResponse }
     * 
     */
    public SaveDataJsonJaxbResponse createSaveDataJsonJaxbResponse() {
        return new SaveDataJsonJaxbResponse();
    }

    /**
     * Create an instance of {@link SaveDataJsonResponse }
     * 
     */
    public SaveDataJsonResponse createSaveDataJsonResponse() {
        return new SaveDataJsonResponse();
    }

    /**
     * Create an instance of {@link SaveDataXml }
     * 
     */
    public SaveDataXml createSaveDataXml() {
        return new SaveDataXml();
    }

    /**
     * Create an instance of {@link SaveDataXmlJaxb }
     * 
     */
    public SaveDataXmlJaxb createSaveDataXmlJaxb() {
        return new SaveDataXmlJaxb();
    }

    /**
     * Create an instance of {@link SaveDataXmlJaxbResponse }
     * 
     */
    public SaveDataXmlJaxbResponse createSaveDataXmlJaxbResponse() {
        return new SaveDataXmlJaxbResponse();
    }

    /**
     * Create an instance of {@link SaveDataXmlResponse }
     * 
     */
    public SaveDataXmlResponse createSaveDataXmlResponse() {
        return new SaveDataXmlResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.kopylov.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUrl }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.kopylov.ru/", name = "getUrl")
    public JAXBElement<GetUrl> createGetUrl(GetUrl value) {
        return new JAXBElement<GetUrl>(_GetUrl_QNAME, GetUrl.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUrlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.kopylov.ru/", name = "getUrlResponse")
    public JAXBElement<GetUrlResponse> createGetUrlResponse(GetUrlResponse value) {
        return new JAXBElement<GetUrlResponse>(_GetUrlResponse_QNAME, GetUrlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataBin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.kopylov.ru/", name = "loadDataBin")
    public JAXBElement<LoadDataBin> createLoadDataBin(LoadDataBin value) {
        return new JAXBElement<LoadDataBin>(_LoadDataBin_QNAME, LoadDataBin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataBinResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.kopylov.ru/", name = "loadDataBinResponse")
    public JAXBElement<LoadDataBinResponse> createLoadDataBinResponse(LoadDataBinResponse value) {
        return new JAXBElement<LoadDataBinResponse>(_LoadDataBinResponse_QNAME, LoadDataBinResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataJson }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.kopylov.ru/", name = "loadDataJson")
    public JAXBElement<LoadDataJson> createLoadDataJson(LoadDataJson value) {
        return new JAXBElement<LoadDataJson>(_LoadDataJson_QNAME, LoadDataJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataJsonJaxb }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.kopylov.ru/", name = "loadDataJsonJaxb")
    public JAXBElement<LoadDataJsonJaxb> createLoadDataJsonJaxb(LoadDataJsonJaxb value) {
        return new JAXBElement<LoadDataJsonJaxb>(_LoadDataJsonJaxb_QNAME, LoadDataJsonJaxb.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataJsonJaxbResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.kopylov.ru/", name = "loadDataJsonJaxbResponse")
    public JAXBElement<LoadDataJsonJaxbResponse> createLoadDataJsonJaxbResponse(LoadDataJsonJaxbResponse value) {
        return new JAXBElement<LoadDataJsonJaxbResponse>(_LoadDataJsonJaxbResponse_QNAME, LoadDataJsonJaxbResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataJsonResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.kopylov.ru/", name = "loadDataJsonResponse")
    public JAXBElement<LoadDataJsonResponse> createLoadDataJsonResponse(LoadDataJsonResponse value) {
        return new JAXBElement<LoadDataJsonResponse>(_LoadDataJsonResponse_QNAME, LoadDataJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.kopylov.ru/", name = "loadDataXml")
    public JAXBElement<LoadDataXml> createLoadDataXml(LoadDataXml value) {
        return new JAXBElement<LoadDataXml>(_LoadDataXml_QNAME, LoadDataXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataXmlJaxb }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.kopylov.ru/", name = "loadDataXmlJaxb")
    public JAXBElement<LoadDataXmlJaxb> createLoadDataXmlJaxb(LoadDataXmlJaxb value) {
        return new JAXBElement<LoadDataXmlJaxb>(_LoadDataXmlJaxb_QNAME, LoadDataXmlJaxb.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataXmlJaxbResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.kopylov.ru/", name = "loadDataXmlJaxbResponse")
    public JAXBElement<LoadDataXmlJaxbResponse> createLoadDataXmlJaxbResponse(LoadDataXmlJaxbResponse value) {
        return new JAXBElement<LoadDataXmlJaxbResponse>(_LoadDataXmlJaxbResponse_QNAME, LoadDataXmlJaxbResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.kopylov.ru/", name = "loadDataXmlResponse")
    public JAXBElement<LoadDataXmlResponse> createLoadDataXmlResponse(LoadDataXmlResponse value) {
        return new JAXBElement<LoadDataXmlResponse>(_LoadDataXmlResponse_QNAME, LoadDataXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataBin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.kopylov.ru/", name = "saveDataBin")
    public JAXBElement<SaveDataBin> createSaveDataBin(SaveDataBin value) {
        return new JAXBElement<SaveDataBin>(_SaveDataBin_QNAME, SaveDataBin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataBinResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.kopylov.ru/", name = "saveDataBinResponse")
    public JAXBElement<SaveDataBinResponse> createSaveDataBinResponse(SaveDataBinResponse value) {
        return new JAXBElement<SaveDataBinResponse>(_SaveDataBinResponse_QNAME, SaveDataBinResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataJson }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.kopylov.ru/", name = "saveDataJson")
    public JAXBElement<SaveDataJson> createSaveDataJson(SaveDataJson value) {
        return new JAXBElement<SaveDataJson>(_SaveDataJson_QNAME, SaveDataJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataJsonJaxb }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.kopylov.ru/", name = "saveDataJsonJaxb")
    public JAXBElement<SaveDataJsonJaxb> createSaveDataJsonJaxb(SaveDataJsonJaxb value) {
        return new JAXBElement<SaveDataJsonJaxb>(_SaveDataJsonJaxb_QNAME, SaveDataJsonJaxb.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataJsonJaxbResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.kopylov.ru/", name = "saveDataJsonJaxbResponse")
    public JAXBElement<SaveDataJsonJaxbResponse> createSaveDataJsonJaxbResponse(SaveDataJsonJaxbResponse value) {
        return new JAXBElement<SaveDataJsonJaxbResponse>(_SaveDataJsonJaxbResponse_QNAME, SaveDataJsonJaxbResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataJsonResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.kopylov.ru/", name = "saveDataJsonResponse")
    public JAXBElement<SaveDataJsonResponse> createSaveDataJsonResponse(SaveDataJsonResponse value) {
        return new JAXBElement<SaveDataJsonResponse>(_SaveDataJsonResponse_QNAME, SaveDataJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.kopylov.ru/", name = "saveDataXml")
    public JAXBElement<SaveDataXml> createSaveDataXml(SaveDataXml value) {
        return new JAXBElement<SaveDataXml>(_SaveDataXml_QNAME, SaveDataXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataXmlJaxb }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.kopylov.ru/", name = "saveDataXmlJaxb")
    public JAXBElement<SaveDataXmlJaxb> createSaveDataXmlJaxb(SaveDataXmlJaxb value) {
        return new JAXBElement<SaveDataXmlJaxb>(_SaveDataXmlJaxb_QNAME, SaveDataXmlJaxb.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataXmlJaxbResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.kopylov.ru/", name = "saveDataXmlJaxbResponse")
    public JAXBElement<SaveDataXmlJaxbResponse> createSaveDataXmlJaxbResponse(SaveDataXmlJaxbResponse value) {
        return new JAXBElement<SaveDataXmlJaxbResponse>(_SaveDataXmlJaxbResponse_QNAME, SaveDataXmlJaxbResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.kopylov.ru/", name = "saveDataXmlResponse")
    public JAXBElement<SaveDataXmlResponse> createSaveDataXmlResponse(SaveDataXmlResponse value) {
        return new JAXBElement<SaveDataXmlResponse>(_SaveDataXmlResponse_QNAME, SaveDataXmlResponse.class, null, value);
    }

}
