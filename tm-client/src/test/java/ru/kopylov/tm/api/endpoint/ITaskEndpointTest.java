package ru.kopylov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.kopylov.tm.endpoint.TaskEndpointService;
import ru.kopylov.tm.endpoint.UserEndpointService;

import java.util.List;

import static org.junit.Assert.*;

public class ITaskEndpointTest {

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @Nullable
    private String adminToken;

    @Nullable
    private String userToken;

    @Before
    public void setUp() {
        try {
            adminToken = userEndpoint.loginUser("test", "test");
        } catch (Exception_Exception e) {
            e.printStackTrace();
        }
        try {
            userEndpoint.persistUser("testUser", "testUser");
            userToken = userEndpoint.loginUser("testUser", "testUser");
            taskEndpoint.createTask(userToken, "testTask000");
        } catch (Exception_Exception e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown() throws Exception_Exception {
        @Nullable final UserDto user = userEndpoint.getUserProfile(userToken);
        @Nullable final UserDto admin = userEndpoint.getUserProfile(adminToken);
        userEndpoint.removeUser(adminToken, user.getId());
//        userEndpoint.removeSession(adminToken, admin.getId());
    }

    @Test
    public void getTaskList() throws Exception_Exception {
        @NotNull List<TaskDto> tasks = taskEndpoint.getTaskList(userToken, null);
        assertFalse(tasks.isEmpty());
        assertEquals("testTask000", tasks.get(0).getName());

        taskEndpoint.clearTask(userToken);
        tasks = taskEndpoint.getTaskList(userToken, null);
        assertTrue(tasks.isEmpty());
    }

    @Test
    public void clearTask() throws Exception_Exception {
        @NotNull List<TaskDto> tasks = taskEndpoint.getTaskList(userToken, null);
        assertFalse(tasks.isEmpty());

        taskEndpoint.clearTask(userToken);
        tasks = taskEndpoint.getTaskList(userToken, null);
        assertTrue(tasks.isEmpty());
    }

    @Test(expected = Exception_Exception.class)
    public void createTaskByGuest() throws Exception_Exception {
        taskEndpoint.createTask(null, "testTask001");
    }

    @Test
    public void createTaskByUser() throws Exception_Exception {
        final boolean createSuccess = taskEndpoint.createTask(userToken, "testTask001");
        Assert.assertTrue(createSuccess);
    }

    @Test
    public void removeTask() throws Exception_Exception {
        @NotNull List<TaskDto> tasks = taskEndpoint.findTaskContent(userToken, "testTask000");
        assertFalse(tasks.isEmpty());

        taskEndpoint.removeTask(userToken, 1);
        tasks = taskEndpoint.findTaskContent(userToken, "testTask000");
        assertTrue(tasks.isEmpty());
    }

    @Test
    public void updateTask() throws Exception_Exception {
        final boolean updateSuccess = taskEndpoint.updateTask(
                userToken,
                1,
                "testTask999",
                "dummy",
                "21.05.2020",
                "25.05.2020",
                1
        );
        assertTrue(updateSuccess);
        assertFalse(taskEndpoint.findTaskContent(userToken, "999").isEmpty());
    }

    @Test
    public void updateTaskNonCorrectState() throws Exception_Exception {
        final boolean updateSuccess = taskEndpoint.updateTask(
                userToken,
                1,
                "testTask999",
                "dummy",
                "21.05.2020",
                "25.05.2020",
                999
        );
        assertFalse(updateSuccess);
        assertTrue(taskEndpoint.findTaskContent(userToken, "999").isEmpty());
    }

    @Test
    public void findTaskContent() throws Exception_Exception {
        @NotNull List<TaskDto> tasks = taskEndpoint.findTaskContent(userToken, "testTask");
        assertFalse(tasks.isEmpty());

        tasks = taskEndpoint.findTaskContent(userToken, "dummy");
        assertTrue(tasks.isEmpty());
    }

}