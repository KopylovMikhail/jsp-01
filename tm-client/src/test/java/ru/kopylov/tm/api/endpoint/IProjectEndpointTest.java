package ru.kopylov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.kopylov.tm.endpoint.ProjectEndpointService;
import ru.kopylov.tm.endpoint.TaskEndpointService;
import ru.kopylov.tm.endpoint.UserEndpointService;

import java.util.List;

public class IProjectEndpointTest {

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @Nullable
    private String adminToken;

    @Nullable
    private String userToken;

    @Before
    public void setUp() {
        try {
            adminToken = userEndpoint.loginUser("test", "test");
        } catch (Exception_Exception e) {
            e.printStackTrace();
        }
        try {
            userEndpoint.persistUser("testUser", "testUser");
            userToken = userEndpoint.loginUser("testUser", "testUser");
            projectEndpoint.createProject(userToken, "testProject000");
            taskEndpoint.createTask(userToken, "testTask000");
        } catch (Exception_Exception e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown() throws Exception_Exception {
        final UserDto user = userEndpoint.getUserProfile(userToken);
        final UserDto admin = userEndpoint.getUserProfile(adminToken);
        userEndpoint.removeUser(adminToken, user.getId());
        projectEndpoint.clearProject(adminToken);
//        userEndpoint.removeSession(adminToken, admin.getId());
    }

    @Test(expected = Exception_Exception.class)
    public void createProjectByGuest() throws Exception_Exception {
        projectEndpoint.createProject(null, "testProject001");
    }

    @Test
    public void createProjectByUser() throws Exception_Exception {
        final boolean createSuccess = projectEndpoint.createProject(userToken, "testProject001");
        Assert.assertTrue(createSuccess);
    }

    @Test(expected = Exception_Exception.class)
    public void setProjectTaskByGuest() throws Exception_Exception {
        projectEndpoint.setProjectTask(null, 1, 1);
    }

    @Test
    public void setProjectTaskByUser() throws Exception_Exception {
        boolean setSuccess = projectEndpoint.setProjectTask(userToken, 1, 1);
        Assert.assertTrue(setSuccess);

        setSuccess = projectEndpoint.setProjectTask(userToken, 999, 1);
        Assert.assertFalse(setSuccess);
    }

    @Test
    public void clearProject() throws Exception_Exception {
        @NotNull List<ProjectDto> projects = projectEndpoint.getProjectList(userToken, null);
        Assert.assertFalse(projects.isEmpty());

        projectEndpoint.clearProject(userToken);
        projects = projectEndpoint.getProjectList(userToken, null);
        Assert.assertTrue(projects.isEmpty());
    }

    @Test
    public void getProjectTaskList() throws Exception_Exception {
        @NotNull List<TaskDto> tasks = projectEndpoint.getProjectTaskList(userToken, 1);
        Assert.assertTrue(tasks.isEmpty());

        tasks = projectEndpoint.getProjectTaskList(userToken, 999);
        Assert.assertTrue(tasks.isEmpty());

        final boolean setSuccess = projectEndpoint.setProjectTask(userToken, 1, 1);
        Assert.assertTrue(setSuccess);
        tasks = projectEndpoint.getProjectTaskList(userToken, 1);
        Assert.assertFalse(tasks.isEmpty());
    }

    @Test
    public void getProjectList() throws Exception_Exception {
        @NotNull List<ProjectDto> projects = projectEndpoint.getProjectList(userToken, null);
        Assert.assertFalse(projects.isEmpty());
        Assert.assertEquals("testProject000", projects.get(0).getName());

        projectEndpoint.clearProject(userToken);
        projects = projectEndpoint.getProjectList(userToken, null);
        Assert.assertTrue(projects.isEmpty());
    }

    @Test
    public void getStateList() {
        @NotNull final List<State> states = projectEndpoint.getStateList();
        Assert.assertFalse(states.isEmpty());
    }

    @Test
    public void findProjectContent() throws Exception_Exception {
        @NotNull List<ProjectDto> projects = projectEndpoint.findProjectContent(userToken, "testProject");
        Assert.assertFalse(projects.isEmpty());

        projects = projectEndpoint.findProjectContent(userToken, "dummy");
        Assert.assertTrue(projects.isEmpty());
    }

    @Test
    public void removeProject() throws Exception_Exception {
        @NotNull List<ProjectDto> projects = projectEndpoint.findProjectContent(userToken, "testProject000");
        Assert.assertFalse(projects.isEmpty());

        projectEndpoint.removeProject(userToken, 1);
        projects = projectEndpoint.findProjectContent(userToken, "testProject000");
        Assert.assertTrue(projects.isEmpty());
    }

    @Test
    public void updateProject() throws Exception_Exception {
        final boolean updateSuccess = projectEndpoint.updateProject(
                userToken,
                1,
                "testProject999",
                "dummy",
                "21.05.2020",
                "25.05.2020",
                1
        );
        Assert.assertTrue(updateSuccess);
        Assert.assertFalse(projectEndpoint.findProjectContent(userToken, "999").isEmpty());
    }

    @Test(expected = Exception_Exception.class)
    public void updateProjectNotCorrectDate() throws Exception_Exception {
        projectEndpoint.updateProject(
                userToken,
                1,
                "testProject999",
                "dummy",
                "2105.2020",
                "25.05.2020",
                1
        );
    }

}